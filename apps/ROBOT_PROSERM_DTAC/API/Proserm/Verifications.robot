*** Settings ***
Suite Setup       Set Suite Proserm Variables
Suite Teardown    Call Save Excel
Library           Collections
Library           String
Library           ExcelLibrary
Library           DateTime
Resource          ../../Resouce/PageKeywords/API/Proserm/CommonKeywords.txt
Resource          ../../Resouce/PageVariables/API/Proserm/ProsermVariables.txt
Library           XML

*** Test Cases ***
TC01_Verifications_:_No_loan
    [Tags]    TC01
    Log    **** Start TC01 Verifications : No Loan ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase1
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase1    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC01 Verifications : No Loan ****

TC02_Verifications_:_Have_loan
    [Tags]    TC02
    Log    **** Start TC02 Verifications : Have Loan ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase2
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase2    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC02 Verifications : Have Loan ****

TC03_Verifications_:_Main_balance_<_0
    [Tags]    TC03
    Log    **** Start TC03 Verifications : Main Balance < 0 ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase3
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase3    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC03 Verifications : Main Balance < 0 ****

TC04_Verifications_:_After_refill_cap_max_>_10000
    [Tags]    TC03
    Log    **** Start TC04 Verifications : After refill cap max > 10,000 ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase4
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase4    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC04 Verifications : After refill cap max > 10,000 ****

TC05_Verifications_:_Status_Idle_on_CBS
    [Tags]    TC03
    Log    **** Start TC05 Verifications : Status Idle on CBS ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase5
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase5    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC05 Verifications : Status Idle on CBS ****

TC06_Verifications_:_Status Pool on CBS
    [Tags]    TC03
    Log    **** Start TC06 Verifications : Status Pool on CBS ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase6
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase6    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC06 Verifications : Status Pool on CBS ****

TC07_Verifications_:_Have_Debt_CPA
    [Tags]    TC03
    Log    **** Start TC07 Verifications : Have Debt CPA ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase7
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase7    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC07 Verifications : Have Debt CPA ****

TC08_Verifications_:_Have_package_conflict
    [Tags]    TC03
    Log    **** Start TC08 Verifications : Have package conflict ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase8
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase8    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC08 Verifications : Have package conflict ****

TC09_Verifications_:_Have_package_block/warn
    [Tags]    TC03
    Log    **** Start TC09 Verifications : Have package block/warn ****
    Log    **** Get Data From Excel ****
    Get Data From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase9
    Log    **** Get Result From Excel ****
    Get Result From Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls
    Log    **** Call Service ****
    Call Service Verifications    ${PROSERM_URL}    ${PROSERM_USERNAME}    ${PROSERM_PASSWORD}    ${PROSERM_PORT}    verifications    &{DATA_DICTIONARY_VAL}
    Log    **** Call End Service ****
    Log    **** Set Verifications Result To Excel ****
    Set Verifications Result To Excel    ${PROSERM_VERIFICATIONS_FILE_NAME}.xls    TestCase9    ${RECEIVE_JSON_VAL}    verifications    &{RESULT_DICTIONARY_VAL}
    Log    **** End TC09 Verifications : Have package block/warn ****
